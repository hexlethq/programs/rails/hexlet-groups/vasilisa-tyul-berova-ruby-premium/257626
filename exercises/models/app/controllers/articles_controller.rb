# frozen_string_literal: true

# top-level documentation comment for class ArticlesController
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end
end
