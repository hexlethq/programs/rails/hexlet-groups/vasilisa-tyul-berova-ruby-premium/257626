# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def before_setup
    @test_arr = %w[first second third fourth fifth]
    @test_stack = Stack.new(@test_arr)
  end

  def test_initialize
    assert { Stack.new.empty? }
  end

  def test_pop
    last_el = @test_stack.pop!
    assert(@test_stack.size == 4)
    assert { last_el == 'fifth' }
  end

  def test_push
    @test_stack.push!('sixth')
    assert { @test_stack.size == 6 }
  end

  def test_empty
    refute { @test_stack.empty? }
    assert { @test_stack.clear!.empty? }
  end

  def test_to_a
    assert { Stack.new.to_a == [] }
    assert { @test_arr == @test_stack.to_a }
  end

  def test_clear
    assert { @test_stack.clear!.empty? }
  end

  def test_size
    assert { Stack.new([]).empty? }
    assert { @test_stack.size == 5 }
    assert { @test_stack.push!('last').size == 6 }
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
