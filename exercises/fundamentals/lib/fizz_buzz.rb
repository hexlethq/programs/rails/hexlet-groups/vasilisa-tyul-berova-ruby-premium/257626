# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = ''
  start.step(to: stop) do |i|
    r = i.to_s
    r = '' if (i % 3).zero? || (i % 5).zero?
    f = (i % 3).zero? ? 'Fizz' : ''
    b = (i % 5).zero? ? 'Buzz' : ''
    result = "#{result}#{f}#{b}#{r} "
  end
  result.strip
end
# END
