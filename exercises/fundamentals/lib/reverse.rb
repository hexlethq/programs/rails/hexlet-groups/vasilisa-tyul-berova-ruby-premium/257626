# frozen_string_literal: true

# BEGIN
def reverse(str)
  resault = ''
  str.each_char do |c|
    resault = "#{c}#{resault}"
  end
  resault
end
# END
