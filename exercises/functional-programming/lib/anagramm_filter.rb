# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, checklist)
  checklist.filter do |el|
    (el.each_char.to_a.sort <=> word.each_char.to_a.sort).eql?(0)
  end
end
# END
