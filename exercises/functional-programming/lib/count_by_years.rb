# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users.each_with_object({}) do |user, acc|
    next unless user[:gender] == 'male'
    year = user[:birthday].split('-').first
    acc[year] = acc.key?(year) ? acc[year] + 1 : 1
  end
end
# END
