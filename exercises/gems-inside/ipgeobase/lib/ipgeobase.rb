# frozen_string_literal: true

require_relative 'ipgeobase/version'
autoload(:IpMeta, 'ipgeobase/ip_meta.rb')

# top-level documentation comment for module Ipgeobase
module Ipgeobase
  ENDPOINT_API = 'http://ip-api.com/xml/'.freeze

  def self.lookup(ip)
    data = Net::HTTP.get(URI("#{ENDPOINT_API}#{ip}"))
    IpMeta.parse(data)
  end
end
