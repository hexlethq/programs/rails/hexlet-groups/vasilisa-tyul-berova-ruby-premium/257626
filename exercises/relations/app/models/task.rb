class Task < ApplicationRecord
  belongs_to :user
  belongs_to :status, dependent: :destroy
end
