# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test '#index' do
    get tasks_path
    assert_response :success
  end

  test '#show' do
    task = tasks(:one)
    get task_path(task)
    assert_response :success
  end

  test '#new' do
    get new_task_path
    assert_response :success
  end

  test '#create' do
    post tasks_path,
         params: {
           task: { name: 'Task Name', description: 'Task Description', creator: 'mrNobody', performer: 'Goldenstern',
                   completed: false }
         }
    assert_response :redirect
  end

  test '#update' do
    task = tasks(:one)
    patch task_path(task), params: { task: { name: 'MyString2' } }
    assert_response :redirect
  end

  test '#edit' do
    task = tasks(:one)
    get edit_task_path(task)
    assert_response :success
  end

  test '#delete' do
    task = tasks(:one)
    delete task_path(task)
    assert_response :redirect
  end
end
