# frozen_string_literal: true

# top-level documentation comment for class ApplicationRecord
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
