# frozen_string_literal: true

# top-level documentation comment for class BulletinsController
class BulletinsController < ApplicationController
  # BEGIN
  def index
    @bulletins = Bulletin.all
  end

  def show
    @bulletin = Bulletin.find(params[:id])
  end
  # END
end
