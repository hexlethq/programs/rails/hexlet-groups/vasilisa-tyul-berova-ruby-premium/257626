# frozen_string_literal: true

# top-level documentation comment for class HomeController
class HomeController < ApplicationController
  def index; end
end
