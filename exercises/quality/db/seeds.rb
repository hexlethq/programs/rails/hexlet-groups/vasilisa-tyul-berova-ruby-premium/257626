# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Bulletin.create([
                  { title: 'Star Wars',
                    body: 'Star Wars Body text body text Body text body text  Body text body text  Body text body te',
                    published: true },
                  { title: 'Lord of the Rings',
                    body: ' Lord of the Rings Body text body text Body text body text  Body text b',
                    published: true },
                  { title: 'What is Lorem Ipsum?',
                    body: 's simply dummy text of the printing and typesetting industry. Lorem Ipsum ',
                    published: true },
                  { title: 'Why do we use it?',
                    body: 't is a long established fact that a reader will be distracted ',
                    published: true }
                ])
