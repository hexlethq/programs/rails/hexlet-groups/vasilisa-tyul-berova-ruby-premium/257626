# frozen_string_literal: true

# top-level documentation comment for class CreateBulletins
class CreateBulletins < ActiveRecord::Migration[6.1]
  def change
    create_table :bulletins do |t|
      t.string :title
      t.text :body
      t.boolean :published

      t.timestamps
    end
  end
end
