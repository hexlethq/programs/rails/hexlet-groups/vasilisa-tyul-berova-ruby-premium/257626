# frozen_string_literal: true

# top-level documentation comment for class SetLocaleMiddleware
class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    I18n.locale = define_locale(env['HTTP_ACCEPT_LANGUAGE'])
    app.call(env)
  end

  private

  def define_locale(header)
    return I18n.default_locale if header.nil?

    locale_from_header = header.scan(/^[a-z]{2}/).first.underscore.to_sym

    I18n.available_locales.include?(locale_from_header) ? locale_from_header : I18n.default_locale
  end
  # END
end
