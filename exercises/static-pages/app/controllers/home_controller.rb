# frozen_string_literal: true

# top-level documentation comment for class PagesController
class HomeController < ApplicationController
  def index; end
end
