# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      registration_page_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users/sign_up')
      submitting_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users')

      registration_page_response = Net::HTTP.get_response(registration_page_uri)
      cookie = registration_page_response.get_fields('set-cookie')

      registration_page_doc = Nokogiri::HTML(registration_page_response.body)
      authenticity_token = registration_page_doc.search('input[name=authenticity_token]').first.attribute('value').value

      request_body = get_request_body(authenticity_token, email, password)

      request = Net::HTTP::Post.new(submitting_uri)
      request['Cookie'] = cookie
      request['Content-Type'] = 'application/x-www-form-urlencoded'
      request.body = URI.encode_www_form request_body

      res = Net::HTTP.start(submitting_uri.hostname, submitting_uri.port, use_ssl: true) do |http|
        http.request request
      end

      case res
      when Net::HTTPRedirection
        puts "Пользовтель успешно создан, доступы #{email} #{password}"
      else
        puts "Email уже существует или ошибка #{res.value}"
      end

      res
    end

    def get_request_body(authenticity_token, email, password)
      {
        'authenticity_token' => authenticity_token,
        'user[email]' => email,
        'user[password]' => password,
        'user[password_confirmation]' => password
      }
    end
    # END
  end
end
