class Task < ApplicationRecord
  validates :name, :status, :creator, :completed, presence: true
  validates :completed, inclusion: { in: [1, 0] }
end
