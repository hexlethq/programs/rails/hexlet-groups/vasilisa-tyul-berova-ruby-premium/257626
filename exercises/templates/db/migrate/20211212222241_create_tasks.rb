class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string 'name'
      t.string 'description'
      t.string 'status', default: 'new'
      t.string 'creator'
      t.string 'performer'
      t.integer 'completed', in: [0, 1]
      t.timestamps
    end
  end
end
