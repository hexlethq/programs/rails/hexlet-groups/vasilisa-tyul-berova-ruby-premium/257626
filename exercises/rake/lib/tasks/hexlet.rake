require 'csv'

namespace :hexlet do
  desc 'TODO'
  task :import_users, [:path] => :environment do |_task, args|
    path = args.path
    abort 'Не указан путь к файлу!' if path.nil?
    abort 'Файл не найден!' unless File.exist?(path)
    CSV.foreach(path, headers: true) do |row|
      User.create(row.to_h)
    end
  end
end
