require 'test_helper'

class Posts::CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = post_comments(:one)
    @post = @comment.post
    @comment_params = {
      body: Faker::Lorem.sentence
    }
  end

  test 'should create comment' do
    post post_comments_path(@post), params: { post_comment: @comment_params }
    comment = PostComment.find_by! @comment_params
    assert_redirected_to post_url(@post)
    assert { comment }
  end

  test 'should destroy comment' do
    delete post_comment_path(@post, @comment)
    assert_redirected_to post_path(@post)
    assert { !PostComment.exists? @comment.id }
  end

  test 'should edit comment' do
    get edit_post_comment_path(@post, @comment)
    assert_response :success
  end

  test 'should update comment' do
    patch post_comment_path(@post, @comment), params: { post_comment: @comment_params }
    assert_redirected_to post_path(@post)
    assert_equal(PostComment.find(@comment.id).body, @comment_params[:body])
  end
end
