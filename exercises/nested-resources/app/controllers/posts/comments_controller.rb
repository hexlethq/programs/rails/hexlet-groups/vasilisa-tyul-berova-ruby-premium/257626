class Posts::CommentsController < ApplicationController
  before_action :set_post, only: %i[create destroy update edit]
  before_action :set_comment, only: %i[destroy update edit]

  def create
    @comment = @post.comments.create(comment_params)
    if @comment.errors.any?
      render 'posts/show', alert: 'There were errors to add comment to post.'
    else
      redirect_to post_path(@post), notice: 'Comment was successfully added.'
    end
  end

  def destroy
    @comment.destroy
    redirect_to post_path(@comment.post), notice: 'Comment was successfully destroyed.'
  end

  def edit; end

  def update
    if @comment.update(comment_params)
      redirect_to post_path(@comment.post), notice: 'Comment was successfully updated.'
    else
      render :edit
    end
  end

  private

  def comment_params
    params.require(:post_comment).permit(:body)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = @post.comments.find(params[:id])
  end
end
