# frozen_string_literal: true

class Post < ApplicationRecord
  # BEGIN
  has_many :comments, dependent: :destroy, class_name: 'PostComment'
  # END

  validates :title, presence: true
  validates :body, length: { maximum: 500 }
end
