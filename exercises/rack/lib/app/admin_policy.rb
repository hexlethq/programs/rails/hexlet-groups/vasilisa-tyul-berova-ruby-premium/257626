class AdminPolicy
  def initialize(app)
    @app = app
    end

  def call(env)
    req = Rack::Request.new(env)
    return [403, {}, []] if req.path.match?(/\/admin.*/)
    @app.call(env)
    end
end
