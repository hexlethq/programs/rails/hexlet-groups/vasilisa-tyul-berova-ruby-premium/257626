class Router
  def initialize
    @code = 200
    @body = ''
  end

  def call(env)
    case Rack::Request.new(env).path
    when '/'
      @body = 'Hello, World!'
    when '/about'
      @body = 'About page'
    else
      @body = '404 Not Found'
      @code = 404
    end
    [@code, {}, [@body]]
  end
end
