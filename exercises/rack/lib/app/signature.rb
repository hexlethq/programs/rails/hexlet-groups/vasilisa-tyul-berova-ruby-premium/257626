require 'digest'

class Signature
  def initialize(app)
    @app = app
    end

  def call(env)
    status, headers, body = @app.call(env)
    append_sha = Digest::SHA256.hexdigest body.first
    [status, headers, body << append_sha]
  end
end
