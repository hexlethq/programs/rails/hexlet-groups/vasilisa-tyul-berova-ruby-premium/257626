# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  params.to_a.sort_by { |el| el[0] }.map do |param, val|
    "#{param}=#{val}"
  end.join('&')
end
# END
# rubocop:enable Style/For
