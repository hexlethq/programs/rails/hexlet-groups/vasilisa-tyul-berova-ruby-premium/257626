# frozen_string_literal: true

def make_censored(text, _stop_words)
  # BEGIN
  template = '$#%!'
  text.split(' ').map do |word|
    _stop_words.include?(word) ? template : word
  end.join(' ')
  # END
end

# rubocop:enable Style/For
