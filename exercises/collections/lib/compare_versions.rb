# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  parts_version1 = version1.split('.').map(&:to_i)
  parts_version2 = version2.split('.').map(&:to_i)
  parts_version1.each_with_index do |item, index|
    return 1 if item > parts_version2[index]
    return -1 if parts_version2[index] > item
  end
  0
end
# END
