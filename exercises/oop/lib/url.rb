# frozen_string_literal: true

# BEGIN
require 'forwardable'
require 'uri'

class Url
  include Comparable
  extend Forwardable
  def_delegators :@url, :scheme, :host, :to_s
  def initialize(url)
    @url = URI(url)
  end

  def query_params
    @url.query.split('&').each_with_object({}) do |i, acc|
      parm, val = i.split('=')
      acc[parm.to_sym] = val
    end
  end

  def query_param(key, default = nil)
    query_params.key?(key) ? query_params[key] : default
  end

  def <=>(other)
    @url.to_s <=> other.to_s
  end
end
# END
